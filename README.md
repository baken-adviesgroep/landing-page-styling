[![pipeline status](https://gitlab.com/baken-adviesgroep/landing-page-styling/badges/master/pipeline.svg)](https://gitlab.com/baken-adviesgroep/landing-page-styling/-/commits/master)

# The deployed page is served under:
https://baken-adviesgroep.gitlab.io/landing-page-styling

# Some Notion page classes to work with:
.notion-page__content {
  margin: 0 auto !important;
  font-family: roboto-mono !important;
}

.notion-page__title {
  display: none !important;
}

.notion-column-list {
  padding: 0px !important;
}



.notion-text__content{
    max-width: 800px !important;
    text-align: left !important;
}
